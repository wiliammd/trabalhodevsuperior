package br.com.trabalho.services;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.trabalho.dto.ClientDTO;
import br.com.trabalho.entities.Client;
import br.com.trabalho.repositories.ClienteRepository;
import br.com.trabalho.services.exceptions.ResourceNotFoundException;

@Service
public class ClientService {
	@Autowired
	private ClienteRepository repository;
	
	@Transactional(readOnly = true)
	public Page<ClientDTO> findAllPaged(PageRequest pageRequest){
		Page<Client> list = repository.findAll(pageRequest);
		return list.map(client -> new ClientDTO(client));
	}
	@Transactional(readOnly = true)
	public ClientDTO findById(Long id) {
		Optional<Client> obj = repository.findById(id);
		Client entity = obj.orElseThrow(()-> new ResourceNotFoundException("Entity Not Found! - Entidade não encontrada!"));
		return new ClientDTO(entity);
	}
	
	@Transactional
	public ClientDTO insert(ClientDTO dto) {
		Client entity = new Client();
		copyToEntity(dto, entity);
		entity = repository.save(entity);
		return new ClientDTO(entity);
	}
	@Transactional
	public ClientDTO update(Long id,ClientDTO dto) {
		try {
		Client entity = repository.getOne(id);
		copyToEntity(dto, entity);
		entity = repository.save(entity);
		return new ClientDTO(entity);
		}catch(EntityNotFoundException e ) {
			throw new ResourceNotFoundException("Id not found - Id não encontrado!" +id);
		}
	}
	@Transactional
	public void delete(long id) {
		try {
			repository.deleteById(id);
			
		}catch(EntityNotFoundException e ) {
			throw new ResourceNotFoundException("Id not found - Id não encontrado!" +id);
		}
	}
	private void copyToEntity(ClientDTO dto, Client entity) {
		entity.setName(dto.getName());
		entity.setCpf(dto.getCpf());
		entity.setIncome(dto.getIncome());
		entity.setBirthDate(dto.getBirthDate());
		entity.setChildren(dto.getChildren());
	}

}
