package br.com.trabalho.dto;

import java.io.Serializable;
import java.time.Instant;

import br.com.trabalho.entities.Client;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class ClientDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private String cpf;
	private Double income;
	private Instant birthDate;
	private Integer children;
	
	public ClientDTO(Client entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.cpf = entity.getCpf();
		this.income = entity.getIncome();
		this.birthDate = entity.getBirthDate();
		this.children = entity.getChildren();
	}
	
	
}
